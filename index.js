const HTTP = require('http');
const PORT = 4000;


HTTP.createServer((req, res) => {

    if(req.url == "/" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to Booking System")

    } else if(req.url == "/profile" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to your profile") 

    }else if(req.url == "/courses" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Here's our courses available") 
    }
    else if(req.url == "/addcourse" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Add a course to our resources") 
    }
    else if(req.url == "/updatecourse" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Update a course to our resources") 
    }
    else if(req.url == "/archivecourses" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Archive courses to our resources") 
    }
    else{
        res.writeHead(404, {"Content-Type": "text/plain"})
        res.end("Bad Request") 
    }

}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));
